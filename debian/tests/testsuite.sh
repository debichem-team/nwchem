#!/bin/bash

set -e

if [ "x$MPI_FLAVOR" = "x" ]; then
  echo "===== TESTING NWCHEM WITH DEFAULT MPI INSTALLATION ====="
  export NWCHEM_EXECUTABLE=/usr/bin/nwchem
else
  echo "===== TESTING NWCHEM BUILD FOR $MPI_FLAVOR ====="
  export MPIRUN_PATH=/usr/bin/mpirun.${MPI_FLAVOR}
  export NWCHEM_EXECUTABLE=/usr/bin/nwchem.${MPI_FLAVOR}
fi


export RUNTESTS=`pwd`/QA/runtests.mpi.unix
export NWCHEM_TOP=`pwd`
export NWCHEM_TARGET=LINUX$(dpkg-architecture -qDEB_HOST_ARCH_BITS)
export NWCHEM_BASIS_LIBRARY=/usr/share/nwchem/libraries/
export NWCHEM_NWPW_LIBRARY=/usr/share/nwchem/libraryps/

(cd QA; ../debian/testsuite)
